<?php
header('Access-Control-Allow-Origin: *');  
	//Autoload
	$loader = require 'vendor/autoload.php';

	//Instanciando objeto
	$app = new \Slim\Slim(array(
	    'templates.path' => 'templates'
	));
	$mail = new PHPMailer(true);

	#REGION TipoObra
		$app->get('/tipoobra/', function() use ($app){
			(new \controllers\TipoObra($app))->lista();
		});
		$app->get('/tipoobras/', function() use ($app){
			(new \controllers\TipoObra($app))->obratipo();
		});
		
		$app->get('/tipoobra/filtro/', function() use ($app){
			(new \controllers\TipoObra($app))->listarFiltros();
		});

		$app->get('/tipoobra/:id', function($id) use ($app){
			(new \controllers\TipoObra($app))->get($id);
		});


		$app->post('/tipoobra/', function() use ($app){
			(new \controllers\TipoObra($app))->nova();
		});


		$app->put('/tipoobra/:id', function($id) use ($app){
			(new \controllers\TipoObra($app))->editar($id);
		});


		$app->delete('/tipoobra/:id', function($id) use ($app){
			(new \controllers\TipoObra($app))->excluir($id);
		});
		
	#ENDREGION

	#REGION Usuario
		$app->get('/login/:email/:senha', function($email,$senha) use ($app){
			(new \controllers\Usuario($app))->login($email,$senha);
		});
		
		$app->get('/loginGoogle/:id', function($id) use ($app){
			(new \controllers\Usuario($app))->loginGoogle($id);
		});
		
		$app->get('/usuario/detalhar/:id', function($id) use ($app){
			(new \controllers\Usuario($app))->detalharUsuario($id);
		});

		$app->put('/usuario/atualizarPerfil/:id', function($id) use ($app){
			(new \controllers\Usuario($app))->atualizarPerfil($id);
		});

		$app->post('/usuario/registroRapido/', function() use ($app){
			(new \controllers\Usuario($app))->cadastroRapido();
		});

		$app->put('/usuario/alterarSenha/:id', function($id) use ($app){
			(new \controllers\Usuario($app))->alterarSenha($id);
		});

		$app->put('/usuario/perfil/:id', function($id) use ($app){
			(new \controllers\Usuario($app))->atualizarPerfil($id);
		});

		$app->put('/usuario/senha/:id', function($id) use ($app){
			(new \controllers\Usuario($app))->atualizarPerfil($id);
		});
		
		$app->get('/artistasPedentes/', function() use ($app){
			(new \controllers\Usuario($app))->artistasPedentes();
		});
		
		$app->get('/ativarArtistasPedentes/:id', function($id) use ($app,$mail){
			(new \controllers\Usuario($app))->ativarArtistasPedentes($id);
		});
		$app->get('/desativarArtistasPedentes/:id', function($id) use ($app,$mail){
			(new \controllers\Usuario($app))->desativarArtistasPedentes($id);
		});

		$app->get('/artistas/', function() use ($app){
			(new \controllers\Usuario($app))->artistas();
		});
		
		$app->post('/usuario/google', function() use ($app){
			(new \controllers\Usuario($app))->cadastroGoogle();
		});

		$app->get('/usuario/verificarstatusartista/', function() use ($app){
			(new \controllers\Usuario($app))->verificarstatusartista();
		});

		$app->get('/usuario/listarartistadaobra/:id', function($id) use ($app){
			(new \controllers\Usuario($app))->listarartistadaobra($id);	
		});

		$app->get('/usuario/artistasbemavaliados/', function() use ($app){
			(new \controllers\Usuario($app))->artistasbemavaliados();	
		});

		$app->get('/usuario/artistasAtivos/', function() use ($app){
			(new \controllers\Usuario($app))->artistasAtivos();	
		});
		
	#ENDREGION

	#REGION Obra
		$app->get('/obra/', function() use ($app){
			(new \controllers\Obra($app))->lista();
		});

		$app->get('/obra/:id', function($id) use ($app){
			(new \controllers\Obra($app))->get($id);
		});

		$app->get('/obraartista/:id', function($id) use ($app){
			(new \controllers\Obra($app))->listaobras($id);
		});

		$app->post('/obra/', function() use ($app){
			(new \controllers\Obra($app))->inserir();
		});
		
		$app->post('/obra/imagem/:id', function($id) use ($app){
			(new \controllers\Obra($app))->inserirImagem($id);
		});
		
		$app->get('/obra/imagem/', function() use ($app){
			(new \controllers\Obra($app))->getcomimagem();
		});
		
		$app->get('/obra/imagem/:id', function($id) use ($app){
			(new \controllers\Obra($app))->listByIdObra($id);
		});
		$app->get('/usuario/obra/imagem/:id', function($id) use ($app){
			(new \controllers\Obra($app))->listByIdUsario($id);
		});

		$app->put('/obra/:id', function($id) use ($app){
			(new \controllers\Obra($app))->editar($id);
		});

		$app->delete('/obra/teste/:id', function($id) use ($app){
			(new \controllers\Obra($app))->excluir($id);
		});
		
		$app->get('/obra/detalhar/:id', function($id) use ($app){
			(new \controllers\Obra($app))->detalharObra($id);
		});
	#ENDREGION
	
	#REGION atelie
		$app->post('/atelie/:id', function($id) use ($app){
			(new \controllers\Atelie($app))->inserir($id);
		});
		
		$app->get('/atelie/:id', function($id) use ($app){
			(new \controllers\Atelie($app))->listById($id);
		});
		
		$app->get('/artistaatelie/:id', function($id) use ($app){
			(new \controllers\Atelie($app))->listByIdUsuario($id);			
		});
	#ENDREGION

	#REGION CategoriaUsuario
		$app->get('/categoriausuario/', function() use ($app){
			(new \controllers\CategoriaUsuario($app))->lista();
		});
		
		$app->get('/categoriausuario/:id', function($id) use ($app){
			(new \controllers\CategoriaUsuario($app))->get($id);
		});


		$app->post('/categoriausuario/inserir/', function() use ($app){
			(new \controllers\CategoriaUsuario($app))->nova();
		});


		$app->put('/categoriausuario/atualizar/:id', function($id) use ($app){
			(new \controllers\CategoriaUsuario($app))->editar($id);
		});


		$app->delete('/categoriausuario/delete/:id', function($id) use ($app){
			(new \controllers\CategoriaUsuario($app))->excluir($id);
		});
	#ENDREGION

	#REGION Avaliacao
		$app->get('/avaliacoes/:id',function($id) use ($app){
			(new \controllers\Avaliacao($app))->listaravaliacoesArtista($id);
		});
		$app->get('/avaliacoesgrupo/',function() use ($app){
			(new \controllers\Avaliacao($app))->grupoavaliacao();
		});
		$app->post('/avaliacoes/', function() use ($app){
			(new \controllers\Avaliacao($app))->inserir();
		});
		$app->put('/avaliacoes/:id',function($id) use ($app){
			(new \controllers\Avaliacao($app))->editar($id);
		});
		$app->get('/avaliacoes/rank/:id',function($id) use ($app){
			(new \controllers\Avaliacao($app))->getRank($id);
		});
	#ENDREGION

	#REGION MOBILE
		$app->get('/mobile/detalharartista/',function() use ($app){
			(new \controllers\Mobile($app))->listarArtistasObras();
		});
		$app->get('/mobile/detalharartistas/:lang/:long',function($lat,$long) use ($app){
			(new \controllers\Mobile($app))->listarArtistasAtelie($lat,$long);
		});
	#ENDREGION
	
	#REGION ARTISTA
		$app->get('/artista/all', function() use ($app){
			(new \controllers\Artista($app))->getAll();
		});
	#ENDREGION 
	
	#REGION filtros
		$app->get('/filtro/categoria/:id', function($id) use ($app){
			(new \controllers\Filtro($app))->byCategoria($id);
		});
		
		$app->get('/filtro/:id', function($id) use ($app){
			(new \controllers\Filtro($app))->byLike($id);
		});
		
		$app->get('/filtro/all/:categoria/:like', function($categoria,$like) use ($app){
			(new \controllers\Filtro($app))->byAll($categoria,$like);
		});
	#ENDREGION
	
//Rodando aplicação
$app->run();
