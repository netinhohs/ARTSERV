<?php
namespace controllers{

	class Filtro{
		//Atributo para banco de dados
		private $PDO;

		
		//Conectando ao banco de dados
		function __construct(){
			include 'Connection.php';
			$this->PDO = $PD;
		}
	
		public function byCategoria($id){
			global $app;
			$query = $this->PDO->prepare("SELECT usuario.*
			FROM usuario
			INNER JOIN obra ON obra.usu_id = usuario.usu_id
			INNER JOIN categoria_obra ON obra.cat_obra_id = categoria_obra.cat_obra_id
			WHERE usuario.usu_ativo = 1 AND usuario.usu_cadastro_completo = 1 AND obra.cat_obra_id = :id 
			GROUP BY usuario.usu_id ");
			
			$query ->bindValue(':id', $id);
			$query->execute();
			$result = $query->fetchAll(\PDO::FETCH_ASSOC);
			$app->render('padrao.php',["data"=>$result],200); 
		}
		
		public function byLike($id){
			global $app;
			$query = $this->PDO->prepare("SELECT usuario.*
			FROM usuario
			INNER JOIN obra ON obra.usu_id = usuario.usu_id
			WHERE usuario.usu_ativo = 1 AND usuario.usu_cadastro_completo = 1 
			AND obra.obr_titulo LIKE CONCAT('%', :id, '%') OR obra.obr_descricao LIKE CONCAT('%', :id, '%') OR usuario.usu_nome LIKE CONCAT('%', :id, '%') 
			GROUP BY usuario.usu_id ");
			$query ->bindValue(':id', $id);
			
			$query->execute();
			$result = $query->fetchAll(\PDO::FETCH_ASSOC);
			$app->render('padrao.php',["data"=>$result],200); 
		}
		
		public function byAll($categoria, $like){
			global $app;
			$query = $this->PDO->prepare("SELECT usuario.*
			FROM usuario
			INNER JOIN obra ON obra.usu_id = usuario.usu_id
			WHERE usuario.usu_ativo = 1 AND usuario.usu_cadastro_completo = 1 
			AND obra.cat_obra_id = :categoria
			AND obra.obr_titulo LIKE CONCAT('%', :like, '%') OR obra.obr_descricao LIKE CONCAT('%', :like, '%') OR usuario.usu_nome LIKE CONCAT('%', :like, '%') 
			GROUP BY usuario.usu_id");
			$query ->bindValue(':categoria', $categoria);
			$query ->bindValue(':like', $like);
			
			$query->execute();
			$result = $query->fetchAll(\PDO::FETCH_ASSOC);
			$app->render('padrao.php',["data"=>$result],200); 
		}

	
	}
}