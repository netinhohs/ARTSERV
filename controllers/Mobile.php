<?php
namespace controllers{

	class Mobile{
		//Atributo para banco de dados
		private $PDO;

		
		//Conectando ao banco de dados
		function __construct(){
			include 'Connection.php';
			$this->PDO = $PD;
		}       
		
		public function listarArtistasObras(){
            global $app;
			$query = $this->PDO->prepare("SELECT 
			usuario.usu_id,
			usuario.usu_email,
			usuario.usu_genero,
			usuario.usu_nome,
			usuario.usu_data_nascimento,
			usuario.usu_imagem,
			usuario.usu_telefone,
			usuario.usu_celular,
			usuario.usu_descricao 
			FROM usuario
			where usuario.cat_usu_id = 2 AND usuario.usu_ativo = 1");
			$query->execute();
            $result = $query->fetchAll(\PDO::FETCH_ASSOC);
			$obra = ["pessoas"=>[]];
			
			foreach($result as $row => $col){
				$obra['pessoas'][$col['usu_id']] =
				[
				"usu_id" => $col['usu_id'],
				"usu_email" => $col['usu_email'],
				"usu_genero" => $col['usu_genero'],
				"usu_nome" => $col['usu_nome'],
				"usu_data_nascimento" => $col['usu_data_nascimento'],
				"usu_imagem" => $col['usu_imagem'],
				"usu_telefone" => $col['usu_telefone'],
				"usu_celular" => $col['usu_celular'],
				"usu_descricao" => $col['usu_descricao'],
				"obra"=>[],
				"atelie"=>[],
				"avaliacao"=>[]
				];
				$query2 = $this->PDO->prepare('SELECT
				obr_id,
				obr_titulo,
				obr_descricao,
				categoria_obra.cat_obra_descricao
				from obra 
				inner join categoria_obra on categoria_obra.cat_obra_id = obra.cat_obra_id
				where usu_id = '. $col['usu_id']);
				$query2->execute();
				$result2 = $query2->fetchAll(\PDO::FETCH_ASSOC);
				$obras=0;
				
				foreach($result2 as $row2 => $col2){

					$obra['pessoas'][$col['usu_id']]['obra'][$obras] =
					[
					"obr_id" => $col2['obr_id'],
					"obr_titulo"=>$col2['obr_titulo'],
					"obr_descricao" => $col2['obr_descricao'],
					"cat_obra_descricao" => $col2['cat_obra_descricao'],
					"imagens" => []
					];
					

					$query3 = $this->PDO->prepare('select * from imagem_obra where obr_id = '. $col2['obr_id']);
					$query3->execute();
					$result3 = $query3->fetchAll(\PDO::FETCH_ASSOC);
					$imagens=0;
					foreach ($result3 as $row3=> $col3) {
						$obra['pessoas'][$col['usu_id']]['obra'][$obras]['imagens'][$imagens]=
						[
						"img_id" => $col3['img_id'],
						"img_url" => "https://www.doocati.com.br/tcc/client/".$col3['img_url']			
						];
						$imagens++;
					}
					$obras++;

				}

				$query4 = $this->PDO->prepare('select * from atelie where usu_id = '. $col['usu_id']);
				$query4->execute();
				$result4 = $query4->fetchAll(\PDO::FETCH_ASSOC);
				$atelie=0;

				foreach ($result4 as $row4 => $col4) {
					$obra['pessoas'][$col['usu_id']]['atelie'][$atelie] =
					[
					"ate_id" => $col4['ate_id'],
					"ate_nome" => $col4['ate_nome'],
					"ate_endereco" => $col4['ate_endereco'],
					"ate_cep" => $col4['ate_cep'],
					"ate_complemento" => $col4['ate_complemento'],
					"ate_latitude" => $col4['ate_latitude'],
					"ate_longitude" => $col4['ate_longitude'],
					"ate_cidade" => $col4['ate_cidade'],
					"ate_estado" => $col4['ate_estado'],
					"ate_meso_regiao" => $col4['ate_meso_regiao']
					];
					$atelie++;
				}

				$query5 = $this->PDO->prepare('select * from avaliacao where ava_ativo = 1 and usu_id_artista = '. $col['usu_id']);
				$query5->execute();
				$result5 = $query5->fetchAll(\PDO::FETCH_ASSOC);
				$avaliacao=0;

				foreach ($result5 as $row5 => $col5) {
					$obra['pessoas'][$col['usu_id']]['avaliacao'][$avaliacao] =
					[
					"ava_id" => $col5['ava_id'],
					"ava_titulo" => $col5['ava_titulo'],
					"ava_descricao" => $col5['ava_descricao'],
					"ava_nota" => $col5['ava_nota'],
					"ava_ativo" => $col5['ava_ativo'],
					"usu_id_artista" => $col5['usu_id_artista'],
					"usu_id" => $col5['usu_id']
					];
					$avaliacao++;
				}

				
			}
			$app->render('padrao.php',["data"=>$obra],200); 
        }
		public function listarArtistasAtelie($lat,$long){
            global $app;
			$query = $this->PDO->prepare("SELECT 
			usuario.usu_id,
			usuario.usu_email,
			usuario.usu_genero,
			usuario.usu_nome,
			usuario.usu_data_nascimento,
			usuario.usu_imagem,
			usuario.usu_telefone,
			usuario.usu_celular 
			FROM usuario
			INNER JOIN atelie on usuario.usu_id = atelie.usu_id
			where (usuario.cat_usu_id = 2 AND usuario.usu_ativo = 1) 
			AND (atelie.ate_latitude = :lat AND atelie.ate_longitude = :long)");
			$query->bindValue(':lat',$lat);
			$query ->bindValue(':long',$long);
			$query->execute();
            $result = $query->fetchAll(\PDO::FETCH_ASSOC);
			$obra = ["pessoas"=>[]];
			
			foreach($result as $row => $col){
				$obra['pessoas'][$col['usu_id']] =
				[
				"usu_id" => $col['usu_id'],
				"usu_email" => $col['usu_email'],
				"usu_genero" => $col['usu_genero'],
				"usu_nome" => $col['usu_nome'],
				"usu_data_nascimento" => $col['usu_data_nascimento'],
				"usu_imagem" => $col['usu_imagem'],
				"usu_telefone" => $col['usu_telefone'],
				"usu_celular" => $col['usu_celular'],
				"obra"=>[],
				"atelie"=>[],
				"avaliacao"=>[]
				];
				$query2 = $this->PDO->prepare('SELECT
				obr_id,
				obr_descricao,
				categoria_obra.cat_obra_descricao
				from obra 
				inner join categoria_obra on categoria_obra.cat_obra_id = obra.cat_obra_id
				where usu_id = '. $col['usu_id']);
				$query2->execute();
				$result2 = $query2->fetchAll(\PDO::FETCH_ASSOC);
				$obras=0;
				
				foreach($result2 as $row2 => $col2){

					$obra['pessoas'][$col['usu_id']]['obra'][$obras] =
					[
					"obr_id" => $col2['obr_id'],
					"obr_descricao" => $col2['obr_descricao'],
					"cat_obra_descricao" => $col2['cat_obra_descricao'],
					"imagens" => []
					];
					

					$query3 = $this->PDO->prepare('select * from imagem_obra where obr_id = '. $col2['obr_id']);
					$query3->execute();
					$result3 = $query3->fetchAll(\PDO::FETCH_ASSOC);
					$imagens=0;
					foreach ($result3 as $row3=> $col3) {
						$obra['pessoas'][$col['usu_id']]['obra'][$obras]['imagens'][$imagens]=
						[
						"img_id" => $col3['img_id'],
						"img_url" => "https://www.doocati.com.br/tcc/client/img/".$col3['img_url']			
						];
						$imagens++;
					}
					$obras++;

				}

				$query4 = $this->PDO->prepare('select * from atelie where usu_id = '. $col['usu_id']);
				$query4->execute();
				$result4 = $query4->fetchAll(\PDO::FETCH_ASSOC);
				$atelie=0;

				foreach ($result4 as $row4 => $col4) {
					$obra['pessoas'][$col['usu_id']]['atelie'][$atelie] =
					[
					"ate_id" => $col4['ate_id'],
					"ate_nome" => $col4['ate_nome'],
					"ate_endereco" => $col4['ate_endereco'],
					"ate_cep" => $col4['ate_cep'],
					"ate_complemento" => $col4['ate_complemento'],
					"ate_latitude" => $col4['ate_latitude'],
					"ate_longitude" => $col4['ate_longitude'],
					"ate_cidade" => $col4['ate_cidade'],
					"ate_estado" => $col4['ate_estado'],
					"ate_meso_regiao" => $col4['ate_meso_regiao']
					];
					$atelie++;
				}

				$query5 = $this->PDO->prepare('select * from avaliacao where ava_ativo = 1 and usu_id_artista = '. $col['usu_id']);
				$query5->execute();
				$result5 = $query5->fetchAll(\PDO::FETCH_ASSOC);
				$avaliacao=0;

				foreach ($result5 as $row5 => $col5) {
					$obra['pessoas'][$col['usu_id']]['avaliacao'][$avaliacao] =
					[
					"ava_id" => $col5['ava_id'],
					"ava_titulo" => $col5['ava_titulo'],
					"ava_descricao" => $col5['ava_descricao'],
					"ava_nota" => $col5['ava_nota'],
					"ava_ativo" => $col5['ava_ativo'],
					"usu_id_artista" => $col5['usu_id_artista'],
					"usu_id" => $col5['usu_id']
					];
					$avaliacao++;
				}

				
			}
			$app->render('padrao.php',["data"=>$obra],200); 
        }
    }
}