<?php
namespace controllers{

	class Avaliacao{
		//Atributo para banco de dados
		private $PDO;

		
		//Conectando ao banco de dados
		function __construct(){
			include 'Connection.php';
			$this->PDO = $PD;
		}
		
		public function inserir()
		{
			global $app;
			
			$dados = json_decode($app->request->getBody(), true);

			$query = $this->PDO->prepare("INSERT INTO avaliacao(ava_titulo,ava_descricao,ava_nota,ava_ativo,usu_id_artista,usu_id) VALUES(:ava_titulo,:ava_descricao,:ava_nota,:ava_ativo,:usu_id_artista,:usu_id)");

			$query ->bindValue(':ava_titulo', $dados['ava_titulo']);
			$query ->bindValue(':ava_descricao', $dados['ava_descricao']);
			$query ->bindValue(':ava_nota', $dados['ava_nota']);
			$query ->bindValue(':ava_ativo', $dados['ava_ativo']);
			$query ->bindValue(':usu_id_artista', $dados['usu_id_artista']);
			$query ->bindValue(':usu_id', $dados['usu_id']);

			$query->execute();
			//Retorna o id inserido
			$app->render('padrao.php',["data"=>['id'=>$this->PDO->lastInsertId()]],200);

		}
		  public function editar($id){
			global $app;

			$dados = json_decode($app->request->getBody(), true);
				$query = $this->PDO->prepare("UPDATE avaliacao SET ava_titulo = :ava_titulo, ava_descricao = :ava_descricao, ava_nota = :ava_nota, ava_ativo = :ava_ativo, usu_id_artista = :usu_id_artista
				, usu_id = :usu_id   WHERE ava_id = :id");
				$query ->bindValue(':ava_titulo', $dados['ava_titulo']);
				$query ->bindValue(':ava_descricao', $dados['ava_descricao']);
				$query ->bindValue(':ava_nota', $dados['ava_nota']);
				$query ->bindValue(':ava_ativo', $dados['ava_ativo']);
				$query ->bindValue(':usu_id_artista', $dados['usu_id_artista']);
				$query ->bindValue(':usu_id', $dados['usu_id']);
				$query ->bindValue(':id',$id);
				
				//Retorna status da edição
				$app->render('padrao.php',["data"=>['status'=>$query->execute() == 1 ]],200); 	
		}		
		

		public function listaravaliacoesArtista($id)
		{
			global $app;
			$query = $this->PDO->prepare("SELECT avaliacao.*, usuario.usu_nome FROM avaliacao, usuario
				WHERE usuario.usu_id = avaliacao.usu_id AND avaliacao.usu_id_artista = :id");
			$query->bindValue(':id', $id);
			$query->execute();
			$result = $query->fetchAll(\PDO::FETCH_ASSOC);
			$app->render('padrao.php',["data"=>$result],200); 
		}

		public function grupoavaliacao()
		{
			global $app;
			$query = $this->PDO->prepare("
				SELECT
				ava_nota,
				COUNT(ava_id) as 'y'
				FROM avaliacao
				GROUP BY ava_nota");
			$query->execute();
			$result = $query->fetchAll(\PDO::FETCH_ASSOC);
			$app->render('padrao.php',["data"=>$result],200); 
		}
		
		public function getRank($id){
			global $app;
			$query = $this->PDO->prepare("SELECT SUM(avaliacao.ava_nota) AS nota, count(*) AS qtd
			FROM usuario, avaliacao
			WHERE usuario.usu_id = avaliacao.usu_id_artista AND usuario.usu_id = :id ");
			$query ->bindValue(':id', $id);
			$query->execute();
			$result = $query->fetch(\PDO::FETCH_ASSOC);
			$app->render('padrao.php',["data"=>$result],200); 
		}
		
	}
}