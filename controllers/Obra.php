<?php
namespace controllers{

	class Obra{
		//Atributo para banco de dados
		private $PDO;

		
		//Conectando ao banco de dados
		function __construct(){
			include 'Connection.php';
			$this->PDO = $PD;
		}
		
		public function lista(){
			$query = $this->PDO->prepare("SELECT obr_id, obr_descricao, cat_obra_descricao FROM obra inner join categoria_obra on obra.cat_obra_id = categoria_obra.cat_obra_id ");
			global $app;
			$query->execute();
			$result = $query->fetchAll(\PDO::FETCH_ASSOC);
			$app->render('padrao.php',["data"=>$result],200); 
			// render função do framework, 1 parametro define o layout que vai ser exportado, 2 a data (conteudo do banco)
			// o valor do status (200 sucesso, 404 erro e etc.)
		}
		//Função para listar obras do aristas requisitado
		public function listaobras($id){
			global $app;
			$query = $this->PDO->prepare("SELECT obr_id, obr_descricao, cat_obra_descricao FROM obra inner join categoria_obra on obra.cat_obra_id = categoria_obra.cat_obra_id
			where usu_id = :id ");
			$query->bindValue(':id', $id);
			$query->execute();
			$result = $query->fetchAll(\PDO::FETCH_ASSOC);
			$app->render('padrao.php',["data"=>$result],200); 
			// render função do framework, 1 parametro define o layout que vai ser exportado, 2 a data (conteudo do banco)
			// o valor do status (200 sucesso, 404 erro e etc.)
		}
		

        // função para listar a obra passando o id
		public function get($id){
			global $app;
			$query = $this->PDO->prepare("SELECT obr_id, obr_descricao, cat_obra_descricao FROM obra inner join categoria_obra on obra.cat_obra_id = categoria_obra.cat_obra_id
			where obr_id = :id ");
			$query ->bindValue(':id',$id);
			$query->execute();
			$result = $query->fetch(\PDO::FETCH_ASSOC);
			$app->render('padrao.php',["data"=>$result],200); 
		}

		public function getcomimagem(){
			global $app;
			$query = $this->PDO->prepare("SELECT obra.obr_id, obr_titulo, obr_descricao, cat_obra_descricao, img.img_id, img.img_url, atelie.ate_bairro, atelie.ate_endereco, atelie.ate_cidade, atelie.ate_estado  
			FROM obra
			INNER JOIN categoria_obra ON obra.cat_obra_id = categoria_obra.cat_obra_id
			LEFT JOIN atelie ON atelie.obr_id = obra.obr_id
			LEFT JOIN imagem_obra img ON img.obr_id = obra.obr_id");
			$query->execute();
			$result = $query->fetchAll(\PDO::FETCH_ASSOC);
			$app->render('padrao.php',["data"=>$result],200); 
		}
		
		public function listByIdUsario($id){
			global $app;
			$query = $this->PDO->prepare("SELECT obra.obr_id, obr_titulo, obr_descricao, cat_obra_descricao, img.img_id, img.img_url, atelie.ate_bairro, atelie.ate_endereco, atelie.ate_cidade, atelie.ate_estado
			FROM obra
			INNER JOIN categoria_obra ON obra.cat_obra_id = categoria_obra.cat_obra_id
			LEFT JOIN atelie ON atelie.obr_id = obra.obr_id
			LEFT JOIN imagem_obra img ON img.obr_id = obra.obr_id
			WHERE obra.usu_id = :id ");
			
			$query ->bindValue(':id',$id);
			
			$query->execute();
			$result = $query->fetchAll(\PDO::FETCH_ASSOC);
			$app->render('padrao.php',["data"=>$result],200); 
		}

		public function listByIdObra($id){
			global $app;
			$query = $this->PDO->prepare("SELECT obra.obr_id, obr_titulo, obr_descricao, cat_obra_descricao, img.img_id, img.img_url, atelie.ate_bairro, atelie.ate_endereco, atelie.ate_cidade, atelie.ate_estado
			FROM obra
			INNER JOIN categoria_obra ON obra.cat_obra_id = categoria_obra.cat_obra_id
			LEFT JOIN atelie ON atelie.obr_id = obra.obr_id
			LEFT JOIN imagem_obra img ON img.obr_id = obra.obr_id
			WHERE obra.obr_id = :id ");
			
			$query ->bindValue(':id',$id);
			
			$query->execute();
			$result = $query->fetchAll(\PDO::FETCH_ASSOC);
			$app->render('padrao.php',["data"=>$result],200); 
		}
		
		public function inserir(){
			global $app;
			
			$dados = json_decode($app->request->getBody(), true); // pega o dado vindo pela requisição
			$query = $this->PDO->prepare("INSERT INTO obra (obr_titulo, obr_descricao, cat_obra_id, usu_id) VALUES (:titulo, :descricao, :codcategoria, :codusuario) ");
			
			$query ->bindValue(':titulo', $dados['obr_titulo']);
			$query ->bindValue(':descricao', $dados['obr_descricao']);
			$query ->bindValue(':codcategoria', $dados['cat_obra_id']);
			$query ->bindValue(':codusuario', $dados['usu_id']);
			
			$query->execute();
			//Retorna o id inserido
			$app->render('padrao.php',["data"=>['id'=>$this->PDO->lastInsertId()]],200);
			
			
		}
		
		public function inserirImagem($id){
			global $app;
			
			$dados = json_decode($app->request->getBody(), true);
			$teste = null;
			foreach ($dados as $key){
				$query = $this->PDO->prepare("INSERT INTO imagem_obra (obr_id, img_url) VALUES (:id, :nome) ");
				$query ->bindValue(':id', $id);
				$query ->bindValue(':nome', $key);
				
				$query->execute();
				$teste = $key;
			}
			
			$app->render('padrao.php',["data"=>['id'=>$this->PDO->lastInsertId()]],200);
		}
	

        public function editar($id){
			global $app;

			$dados = json_decode($app->request->getBody(), true);
			if(validardados('UPDATE', $dados)){
				$query = $this->PDO->prepare("UPDATE obra SET obra_descricao = :description, cat_obra_id = :tipoobra WHERE obr_id = :id");
				$query ->bindValue(':description', $dados['obra_descricao']);
				$query ->bindValue(':tipoobra', $dados['cat_obra_id']);
				$query ->bindValue(':id',$id);
				
				//Retorna status da edição
				$app->render('padrao.php',["data"=>['status'=>$query->execute() == 1 ]],200); 	
			}else{
				$app->render('padrao.php',["data"=>['status'=>'invaild format']],404);
			}		
		}
		
		public function detalharObra($id){
			global $app;
			
			$dados = array();
			$query = $this->PDO->prepare("SELECT obra.*, categoria_obra.cat_obra_descricao, atelie.*, usuario.*
			FROM obra
			INNER JOIN usuario ON obra.usu_id = usuario.usu_id
			INNER JOIN categoria_obra ON obra.cat_obra_id = categoria_obra.cat_obra_id
			LEFT JOIN atelie ON atelie.obr_id = obra.obr_id
			WHERE obra.obr_id = :id ");
			
			$query ->bindValue(':id',$id);
			
			$query->execute();
			$dados = $query->fetch(\PDO::FETCH_ASSOC);
			
			$query = $this->PDO->prepare(" SELECT * from imagem_obra WHERE obr_id = :id ");
			$query ->bindValue(':id',$id);
			
			$query->execute();
			$dados['imagens'] = $query->fetchAll(\PDO::FETCH_ASSOC);
			
			$app->render('padrao.php',["data"=>$dados],200); 
		}

        public function excluir($id){
			global $app;

			$sth = $this->PDO->prepare("DELETE FROM obra WHERE obr_id = :id ");
			$sth ->bindValue(':id',$id);
			
			//retorna status da exclusão.
			$app->render('padrao.php',["data"=>['status'=>$sth->execute()==1]],200); 
		}
	}
}