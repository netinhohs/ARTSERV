<?php
namespace controllers{

	class Usuario{
		//Atributo para banco de dados
		private $PDO;

		
		//Conectando ao banco de dados
		function __construct(){
			include 'Connection.php';
			//require '../vendor/phpmailer/phpmailer/PHPMailerAutoload.php';
			$this->PDO = $PD;
		}
		
		public function login($email, $senha){
			global $app;
			$query = $this->PDO->prepare("SELECT * FROM usuario WHERE usu_email = :email AND usu_senha = :senha ");
			$query ->bindValue(':email',$email);
			$query ->bindValue(':senha',$senha);
			$query->execute();
			$result = $query->fetch(\PDO::FETCH_ASSOC);
			$app->render('padrao.php',["data"=>$result],200); 
		}
		
		// metodo para listar todas os usuarios de categoria 2(artista) que estão pedentes
		public function artistasPedentes(){
			global $app;
			$query = $this->PDO->prepare("SELECT usuario.*, categoria_usuario.cat_usu_descricao
				FROM usuario, categoria_usuario WHERE usuario.usu_ativo = 0 AND usuario.cat_usu_id = 2
				AND categoria_usuario.cat_usu_id = usuario.cat_usu_id");
			$query->execute();
			$result = $query->fetchAll(\PDO::FETCH_ASSOC);
			$app->render('padrao.php',["data"=>$result],200); 
		}

		// metodo para listar todas os usuarios de categoria 2(artista)
		public function artistas(){
			global $app;
			$query = $this->PDO->prepare("SELECT usuario.*, categoria_usuario.cat_usu_descricao
				FROM usuario, categoria_usuario WHERE usuario.cat_usu_id = 2
				AND categoria_usuario.cat_usu_id = usuario.cat_usu_id");
			$query->execute();
			$result = $query->fetchAll(\PDO::FETCH_ASSOC);
			$app->render('padrao.php',["data"=>$result],200); 
		}
		
		//metodo para ativar o usuarios de categoria 2(artista) que se encontrar pedente, espera receber o id do usuario.
		public function ativarArtistasPedentes($id){
			global $app;
			try {
					$query = $this->PDO->prepare("UPDATE usuario SET usu_ativo = 1 WHERE usu_id = :id");
					$query ->bindValue(':id',$id);
					$query->execute();
					$this->enviaremail($id);
					$app->render('padrao.php',["data"=>['status'=>$query->execute() == 1 ]],200);					
				} catch (Exception $e) {
					$app->render('padrao.php',["data"=>['status'=>$e->getMessage() ]],404);		
				}						
		
			//Retorna status da edição
			
		}
				//metodo para desativar o usuarios de categoria 2(artista) que se encontrar pedente, espera receber o id do usuario.
		public function desativarArtistasPedentes($id){
			global $app;						
			try {
					$query = $this->PDO->prepare("UPDATE usuario SET usu_ativo = 0 WHERE usu_id = :id");
					$query ->bindValue(':id',$id);
					$query->execute();
					$this->enviaremail($id);
					$app->render('padrao.php',["data"=>['status'=>$query->execute() == 1 ]],200);					
				} catch (Exception $e) {
					$app->render('padrao.php',["data"=>['status'=>$e->getMessage() ]],404);		
				}						
		
		}
				//Prepara as informações necessarias para ser enviado o email
		public function enviaremail($id){
			try {
			$query = $this->PDO->prepare("SELECT usu_email, usu_ativo,usu_nome FROM usuario WHERE usu_id = :id ");
			$query ->bindValue(':id', $id);
			$query->execute();
			$result = $query->fetchAll(\PDO::FETCH_ASSOC);
			$email = '';
			$ativo = '';
			$nome = '';
			foreach($result as $row => $col){
				$email = $col['usu_email'];
				$ativo = $col['usu_ativo'];
				$nome = $col['usu_nome'];
			}	

			global $mail;
			if($ativo == 0){
				$this->enviaremailDesativar($mail,$email,$nome);
			}else{
				$this->enviaremailAtivar($mail,$email,$nome);
			}
			
			} catch (Exception $e) {
				echo $e->errorMessage();
			}
			
		}
		//Envia o email para o usuario ativado
		public function enviaremailAtivar($mail,$email,$nome){
			$mail->IsSMTP(); // Define que a mensagem será SMTP
			
			try {
				$mail ->CharSet = "UTF-8";
				$mail->Host = 'smtp.gmail.com'; // Endereço do servidor SMTP (Autenticação, utilize o host smtp.seudomínio.com.br)
				$mail->SMTPAuth   = true;  // Usar autenticação SMTP (obrigatório para smtp.seudomínio.com.br)
				$mail->Port       = 465; //  Usar 587 porta SMTP
				$mail->Username = 'team.eighht@gmail.com'; // Usuário do servidor SMTP (endereço de email)
				$mail->Password = '123456de'; // Senha do servidor SMTP (senha do email usado)
				$mail->SMTPSecure = 'ssl'; 
				//Define o remetente
				// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=    
				$mail->SetFrom('team.eighht@gmail.com', 'Arteson'); //Seu e-mail
				$mail->AddReplyTo('team.eighht@gmail.com', 'Arteson'); //E-mail cópia (coloquei o mesmo para testar com mais facilidade)
				$mail->Subject = 'E-mail de Ativação';//Assunto do e-mail
			
			
				//Define os destinatário(s)
				//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
				$mail->AddAddress($email, $nome);
			
				//Campos abaixo são opcionais 
				//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
				//$mail->AddCC('destinarario@dominio.com.br', 'Destinatario'); // Copia
				//$mail->AddBCC('destinatario_oculto@dominio.com.br', 'Destinatario2`'); // Cópia Oculta
				//$mail->AddAttachment('images/phpmailer.gif');      // Adicionar um anexo
			
			
				//Define o corpo do email
				$mail->Body ='Prezado(a) '.$nome.', O seu cadastro foi ativado do nosso sistema.'; 
			
				////Caso queira colocar o conteudo de um arquivo utilize o método abaixo ao invés da mensagem no corpo do e-mail.
				//$mail->MsgHTML(file_get_contents('arquivo.html'));
			
				$mail->Send();
				
			
				//caso apresente algum erro é apresentado abaixo com essa exceção.
				}catch (Exception $e) {
				echo $e->errorMessage(); //Mensagem de erro costumizada do PHPMailer
			}
		}
		//Envia o email para o usuario desativado
		public function enviaremailDesativar($mail,$email,$nome){
			$mail->IsSMTP(); // Define que a mensagem será SMTP
			
			try {
				$mail ->charSet = "UTF-8";
				$mail->Host = 'smtp.gmail.com'; // Endereço do servidor SMTP (Autenticação, utilize o host smtp.seudomínio.com.br)
				$mail->SMTPAuth   = true;  // Usar autenticação SMTP (obrigatório para smtp.seudomínio.com.br)
				$mail->Port       = 465; //  Usar 587 porta SMTP
				$mail->Username = 'team.eighht@gmail.com'; // Usuário do servidor SMTP (endereço de email)
				$mail->Password = '123456de'; // Senha do servidor SMTP (senha do email usado)
				$mail->SMTPSecure = 'ssl'; 
				
				//Define o remetente
				// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=    
				$mail->SetFrom('team.eighht@gmail.com', 'Arteson'); //Seu e-mail
				$mail->AddReplyTo('team.eighht@gmail.com', 'Arteson'); //E-mail cópia (coloquei o mesmo para testar com mais facilidade)
				$mail->Subject = 'E-mail de Destivação';//Assunto do e-mail
						
				//Define os destinatário(s)
				//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
				$mail->AddAddress($email, $nome);
			
				//Campos abaixo são opcionais 
				//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
				//$mail->AddCC('destinarario@dominio.com.br', 'Destinatario'); // Copia
				//$mail->AddBCC('destinatario_oculto@dominio.com.br', 'Destinatario2`'); // Cópia Oculta
				//$mail->AddAttachment('images/phpmailer.gif');      // Adicionar um anexo
			
			
				//Define o corpo do email
				$mail->Body = 'Prezado(a) '.$nome.', O seu cadastro foi desativado do nosso sistema.';
				
			
				////Caso queira colocar o conteudo de um arquivo utilize o método abaixo ao invés da mensagem no corpo do e-mail.
				//$mail->MsgHTML(file_get_contents('arquivo.html'));
			
				 if(!$mail->Send()) {
				echo "Mailer Error: " . $mail->ErrorInfo;
			} else {
				echo "Message has been sent";
			}
				}catch (phpmailerException $e) {
				echo $e->errorMessage(); //Mensagem de erro costumizada do PHPMailer
			}
		}

		//Cadastrar artistas
		public function nova(){
			global $app;

			$dados = json_decode($app->request->getBody(), true); // pega o dado vindo pela requisição
			$query = $this->PDO->prepare("INSERT INTO usuario (usu_email, usu_cpf, usu_genero, usu_nome, usu_data_nascimento, usu_ativo, cat_usu_id, usu_senha) values (:email, :cpf, :genero, :nome, :datanascimento, :ativo, :categoria, :senha) ");
			$query ->bindValue(':email', $dados['usu_email']);
            $query ->bindValue(':cpf', $dados['usu_cpf']);
			$query ->bindValue(':genero', $dados['usu_genero']);
			$query ->bindValue(':nome', $dados['usu_nome']);
			$query ->bindValue(':datanascimento', $dados['usu_data_nascimento']);
			$query ->bindValue(':ativo', $dados['usu_ativo']);
			$query ->bindValue(':categoria', $dados['cat_usu_id']);
			$query ->bindValue(':senha', $dados['usu_senha']);


			$query->execute();
			//Retorna o id inserido
			$app->render('padrao.php',["data"=>['id'=>$this->PDO->lastInsertId()]],200); 
		}

			//Atualizar artistas
        public function editar($id){
			global $app;

			$dados = json_decode($app->request->getBody(), true);
			
			$query = $this->PDO->prepare("UPDATE usuario SET usu_email = :email, usu_cpf = :cpf, usu_genero = :genero, usu_nome = :nome, usu_data_nascimento = :datanascimento, usu_senha = :senha  WHERE usu_id = :id");
			$query ->bindValue(':email', $dados['usu_email']);
            $query ->bindValue(':cpf', $dados['usu_cpf']);
			$query ->bindValue(':genero', $dados['usu_genero']);
			$query ->bindValue(':nome', $dados['usu_nome']);
			$query ->bindValue(':datanascimento', $dados['usu_data_nascimento']);
			$query ->bindValue(':senha', $dados['usu_senha']);
			$query ->bindValue(':id',$id);
			$query->execute();
			//Retorna status da edição do usuario
			$app->render('padrao.php',["data"=>['status'=>$query->execute() == 1 ]],200);
		}

			//Excluir artistas

        public function excluir($id){
			global $app;

			$sth = $this->PDO->prepare("DELETE FROM usuario WHERE usu_id = :id");
			$sth ->bindValue(':id',$id);
			
			//retorna status da exclusão.
			$app->render('padrao.php',["data"=>['status'=>$sth->execute()==1]],200); 
		}

		public function cadastroGoogle(){
			global $app;

			$dados = json_decode($app->request->getBody(), true);
			$query = $this->PDO->prepare("INSERT INTO usuario (usu_email, usu_genero, usu_nome, usu_ativo, cat_usu_id, usu_senha, usu_id_google, usu_cadastro_completo, usu_imagem) 
							values (:email, '', :nome, 0, 2, '123', :idgoogle, 0, :imagem) ");
			$query ->bindValue(':email', $dados['usu_email']);
			$query ->bindValue(':nome', $dados['usu_nome']);
			$query ->bindValue(':idgoogle', $dados['usu_id_google']);
			$query ->bindValue(':imagem', $dados['usu_imagem']);

			$query->execute();
			$app->render('padrao.php',["data"=>['id'=>$this->PDO->lastInsertId()]],200); 	
		}
		
		public function loginGoogle($id){
			global $app;
			$query = $this->PDO->prepare("SELECT * FROM usuario WHERE usu_id_google = :id");
			$query ->bindValue(':id',$id);
			$query->execute();
			
			$result = $query->fetch(\PDO::FETCH_ASSOC);
			$app->render('padrao.php',["data"=>$result],200); 
		}
		
		public function detalharUsuario($id){
			global $app;
			
			$query = $this->PDO->prepare("SELECT 
				usuario.*, 
				count(obra.obr_id) as obras 
				FROM usuario
				INNER JOIN obra on obra.usu_id = usuario.usu_id 
				WHERE usuario.usu_id = :id ");
			$query ->bindValue(':id',$id);
			$query->execute();
			$result = $query->fetch(\PDO::FETCH_ASSOC);
			$app->render('padrao.php',["data"=>$result],200); 
		}

		public function atualizarPerfil($id){
			global $app;

			$dados = json_decode($app->request->getBody(), true);
			if(isset($dados)){
				$controImg = "";
				if(!empty($dados['usu_imagem'])){
						$controImg = " usu_imagem = :imagem, ";
				}
				if(empty($dados['usu_cadastro_completo'])){
					$query = $this->PDO->prepare("UPDATE usuario SET usu_cpf = :cpf, usu_genero = :genero, usu_nome = :nome,
						usu_cadastro_completo = true, usu_telefone = :telefone, ".$controImg."
						usu_descricao = :descricao, usu_celular = :celular WHERE usu_id = :id ");
				$query ->bindValue(':genero', $dados['usu_genero']);
				}			
				else{
					$query = $this->PDO->prepare("UPDATE usuario SET usu_cpf = :cpf, usu_nome = :nome, 	
						usu_telefone = :telefone, usu_celular = :celular, ".$controImg." 
						usu_descricao = :descricao WHERE usu_id = :id ");
				}
				if(!empty($dados['usu_imagem'])){
					$controImg = $query ->bindValue(':imagem', $dados['usu_imagem']);
				}
				$query ->bindValue(':cpf', $dados['usu_cpf']);
				$query ->bindValue(':nome', $dados['usu_nome']);
				$query ->bindValue(':descricao', $dados['usu_descricao']);
				$query ->bindValue(':telefone', $dados['usu_telefone']);
				$query ->bindValue(':celular', $dados['usu_celular']);
				$query ->bindValue(':id', $id);	

				$app->render('padrao.php',["data"=>['status'=>$query->execute() == 1 ]],200);
			} 
			else {
				$app->render('padrao.php',["data"=>['status'=>FALSE ]],404);
			}
		}
		
		public function alterarSenha($id){
			global $app;

			$dados = json_decode($app->request->getBody(), true);
			$query = $this->PDO->prepare("UPDATE usuario SET usu_senha = :senha WHERE usu_id = :id ");

			$query ->bindValue(':senha', $dados);
			$query ->bindValue(':id', $id);

			$app->render('padrao.php',["data"=>['status'=>$query->execute() == 1 ]],200);
		}

		public function cadastroRapido(){
			global $app;

			$dados = json_decode($app->request->getBody(), true);
			$query = $this->PDO->prepare("INSERT INTO usuario (usu_email, usu_nome, usu_senha, usu_ativo, cat_usu_id, usu_cadastro_completo) 
					VALUES (:email, :nome, :senha, 0, 2, 0) ");
			$query ->bindValue(':email', $dados['usu_email']);
			$query ->bindValue(':nome', $dados['usu_nome']);
			$query ->bindValue(':senha', $dados['usu_senha']);

			$query->execute();
			//Retorna o id inserido
			$app->render('padrao.php',["data"=>['id'=>$this->PDO->lastInsertId()]],200); 
		}


		//verificar se o artista está ativo

		public function verificarstatusartista ($id){
			global $app;						
			$query = $this->PDO->prepare("select usu_ativo from usuario WHERE usu_id = :id");
			$query ->bindValue(':id',$id);							
			//Retorna status do Artista
			$query->execute();
			$result = $query->fetchAll(\PDO::FETCH_ASSOC);
			$app->render('padrao.php',["data"=>$result],200);
		}


		//listar artista de uma determinada obra no detalhe
        
 
		public function listarartistadaobra($id){
			global $app;
			$query = $this->PDO->prepare("SELECT usuario.usu_id, usu_nome, usu_email FROM usuario inner join obra on obra.usu_id = usuario.usu_id
			where obra.obr_id = :id ");
			$query ->bindValue(':id',$id);
			$query->execute();
			$result = $query->fetch(\PDO::FETCH_ASSOC);
			$app->render('padrao.php',["data"=>$result],200); 
		}	


		public function artistasbemavaliados(){
			global $app;
			$query = $this->PDO->prepare("SELECT usuario.usu_id, usu_nome, usu_email, ava_titulo, ava_descricao, avg(ava_nota) as media FROM usuario
            INNER JOIN avaliacao ON usuario.usu_id = avaliacao.usu_id_artista WHERE usuario.cat_usu_id = 2 GROUP BY avaliacao.usu_id_artista
            ORDER BY avaliacao.ava_nota desc");
			$query->execute();
			$result = $query->fetchAll(\PDO::FETCH_ASSOC);
			$app->render('padrao.php',["data"=>$result],200); 
		}	

		public function artistasAtivos(){
			global $app;
			$query = $this->PDO->prepare("SELECT usuario.*, categoria_usuario.cat_usu_descricao
				FROM usuario, categoria_usuario WHERE usuario.usu_ativo = 1 AND usuario.cat_usu_id = 2
				AND categoria_usuario.cat_usu_id = usuario.cat_usu_id");
			$query->execute();
			$result = $query->fetchAll(\PDO::FETCH_ASSOC);
			$app->render('padrao.php',["data"=>$result],200); 
		}


	}

}