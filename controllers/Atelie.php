<?php
namespace controllers{

	class Atelie{
		//Atributo para banco de dados
		private $PDO;

		
		//Conectando ao banco de dados
		function __construct(){
			include 'Connection.php';
			$this->PDO = $PD;
		}
		
		public function inserir($id){
			global $app;
			
			$dados = json_decode($app->request->getBody(), true); // pega o dado vindo pela requisição
			$query = $this->PDO->prepare("INSERT INTO atelie (ate_nome, ate_numero, ate_bairro, ate_endereco, ate_cep, ate_complemento, ate_latitude, ate_longitude, ate_cidade, 
				ate_estado, ate_meso_regiao, usu_id, obr_id) VALUES (:ate_nome, :ate_numero, :ate_bairro, :ate_endereco, :ate_cep, :ate_complemento, :ate_latitude, :ate_longitude, :ate_cidade, :ate_estado, :ate_meso_regiao, :usu_id, :id) ");
			
			$query ->bindValue(':ate_nome', $dados['ate_nome']);
			$query ->bindValue(':ate_endereco', $dados['ate_endereco']);
			$query ->bindValue(':ate_bairro', $dados['ate_bairro']);
			$query ->bindValue(':ate_numero', $dados['ate_numero']);
			$query ->bindValue(':ate_cep', $dados['ate_cep']);
			$query ->bindValue(':ate_complemento', $dados['ate_complemento']);
			$query ->bindValue(':ate_latitude', $dados['ate_latitude']);
			$query ->bindValue(':ate_longitude', $dados['ate_longitude']);
			$query ->bindValue(':ate_cidade', $dados['ate_cidade']);
			$query ->bindValue(':ate_estado', $dados['ate_estado']);
			$query ->bindValue(':ate_meso_regiao', $dados['ate_meso_regiao']);
			$query ->bindValue(':usu_id', $dados['usu_id']);
			$query ->bindValue(':id', $id);
			
			$query->execute();
			//Retorna o id inserido
			$app->render('padrao.php',["data"=>['id'=>$this->PDO->lastInsertId()]],200);
			
		}
		
		public function listById($id){
			global $app;
			$query = $this->PDO->prepare("SELECT * FROM atelie WHERE obr_id = :id  ");
			$query ->bindValue(':id', $id);
			$query->execute();
			$result = $query->fetchAll(\PDO::FETCH_ASSOC);
			$app->render('padrao.php',["data"=>$result],200); 
		}

        // Retorna o Atelie com o Id do Usuario 
		public function listByIdUsuario($id){
			global $app;
			$query = $this->PDO->prepare("SELECT * FROM atelie WHERE usu_id = :id  ");
			$query ->bindValue(':id', $id);
			$query->execute();
			$result = $query->fetchAll(\PDO::FETCH_ASSOC);
			$app->render('padrao.php',["data"=>$result],200); 
		}

	}
}