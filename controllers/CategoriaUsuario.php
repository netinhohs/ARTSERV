<?php
namespace controllers{
    
    class CategoriaUsuario{
        //Atributo para banco de dados
        private $PDO;
        
        
        //Conectando ao banco de dados
        function __construct(){
            include 'Connection.php';
			$this->PDO = $PD;
        }
        
        // função para listar todas os tipos de obra
        public function lista(){
            $query = $this->PDO->prepare("SELECT * FROM categoria_usuario");
            global $app;
            $query->execute();
            $result = $query->fetchAll(\PDO::FETCH_ASSOC);
            $app->render('padrao.php',["data"=>$result],200);
            // render função do framework, 1 parametro define o layout que vai ser exportado, 2 a data (conteudo do banco)
            // o valor do status (200 sucesso, 404 erro e etc.)
        }
        
        // função para listar os tipos de obra passando o id
        public function get($id){
            global $app;
            $query = $this->PDO->prepare("SELECT * FROM categoria_usuario WHERE cat_usu_id = :id ");
            $query ->bindValue(':id',$id);
            $query->execute();
            $result = $query->fetch(\PDO::FETCH_ASSOC);
            $app->render('padrao.php',["data"=>$result],200);
        }
		
		public function nova(){
			global $app;

			$dados = json_decode($app->request->getBody(), true); // pega o dado vindo pela requisição
			$query = $this->PDO->prepare("INSERT INTO categoria_usuario (cat_usu_descricao) values (:nome) ");
			$query ->bindValue(':nome', $dados['cat_usu_descricao']);

			$query->execute();
			//Retorna o id inserido
			$app->render('padrao.php',["data"=>['id'=>$this->PDO->lastInsertId()]],200); 
		}
		
		public function editar($id){
			global $app;

			$dados = json_decode($app->request->getBody(), true);
			
			$query = $this->PDO->prepare("UPDATE categoria_usuario SET cat_usu_descricao = :nome WHERE cat_usu_id = :id ");
			$query ->bindValue(':nome',$dados['cat_usu_descricao']);

			$query ->bindValue(':id',$id);
			
			//Retorna status da edição
			$app->render('padrao.php',["data"=>['status'=>$query->execute() == 1 ]],200); 			
		}
		
		public function excluir($id){
			global $app;

			$sth = $this->PDO->prepare("DELETE FROM categoria_usuario WHERE cat_usu_id = :id ");
			$sth ->bindValue(':id',$id);
			
			//retorna status da exclusão.
			$app->render('padrao.php',["data"=>['status'=>$sth->execute()==1]],200); 
		}
        
        
    }
    
}